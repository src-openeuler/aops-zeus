Name:		aops-zeus
Version:	v2.0.0
Release:	2
Summary:	A service which is the foundation of aops.
License:	MulanPSL2
URL:		https://gitee.com/openeuler/%{name}
Source0:	%{name}-%{version}.tar.gz
Patch0001:	0001-updated-download-host-template-api.patch


BuildRequires:  python3-setuptools
Requires:   python3-pyyaml python3-PyMySQL python3-kazoo python3-click


%description
Provide one-click aops deployment, service start and stop, hot loading of 
configuration files, and database initialization.
Provides:   aops-zeus

%package -n zeus-host-information
Summary: A host manager service which is the foundation of aops.
Requires:  aops-vulcanus >= v2.0.0 aops-zeus >= v2.0.0
Requires:  python3-gevent python3-uWSGI python3-paramiko

%description -n zeus-host-information
A host manager service which is the foundation of aops.

%package -n zeus-user-access
Summary: A user manager service which is the foundation of aops.
Requires:   aops-vulcanus >= v2.0.0 aops-zeus >= v2.0.0
Requires:   python3-celery python3-uWSGI

%description -n zeus-user-access
A user manager service which is the foundation of aops.

%package -n async-task
Summary: A async task of aops.
Requires:   aops-vulcanus >= v2.0.0 python3-celery python3-paramiko


%description -n async-task
A async task of aops.

%package -n zeus-distribute
Summary: A distributed service of aops.
Requires:   aops-vulcanus >= v2.0.0 aops-zeus >= v2.0.0
Requires:   python3-uWSGI python3-gevent
%description -n zeus-distribute
A distributed service of aops.

%prep
%autosetup -n %{name}-%{version} -p1


# build for aops-zeus
%py3_build

# build for zeus-host-information
pushd host-information-service
%py3_build
popd

# build for zeus-user-access
pushd user-access-service
%py3_build
popd

# build for async-task
pushd async-task
%py3_build
popd

# build for zeus-distribute
pushd distribute-service
%py3_build
popd

# install for aops-zeus
%py3_install

# install for zeus-host-information
pushd host-information-service
%py3_install
mkdir -p %{buildroot}/opt/aops/database/
cp zeus/host_information_service/database/*.sql %{buildroot}/opt/aops/database/
popd

# install for zeus-user-access
pushd user-access-service
%py3_install
mkdir -p %{buildroot}/opt/aops/database/
cp zeus/user_access_service/database/*.sql %{buildroot}/opt/aops/database/
popd

# install for async-task
pushd async-task
%py3_install
mkdir -p %{buildroot}/opt/aops/celery
popd

# install for zeus-distribute
pushd distribute-service
%py3_install
popd

%files
%doc README.*
%{python3_sitelib}/aops_zeus*.egg-info
%{python3_sitelib}/zeus/*
%attr(0755,root,root) %{_bindir}/aops-cli

%files -n zeus-host-information
%attr(0644,root,root) %{_sysconfdir}/aops/conf.d/zeus-host-information.yml
%attr(0755,root,root) %{_unitdir}/zeus-host-information.service
%{python3_sitelib}/zeus_host_information*.egg-info/*
%{python3_sitelib}/zeus/host_information_service/*
%attr(0755, root, root) /opt/aops/database/*

%files -n zeus-user-access
%attr(0644,root,root) %{_sysconfdir}/aops/conf.d/zeus-user-access.yml
%attr(0755,root,root) %{_unitdir}/zeus-user-access.service
%{python3_sitelib}/zeus_user_access*.egg-info/*
%{python3_sitelib}/zeus/user_access_service/*
%attr(0755, root, root) /opt/aops/database/*

%files -n async-task
%attr(0644,root,root) %{_sysconfdir}/aops/crontab.yml
%attr(0644,root,root) %{_sysconfdir}/aops/sync-conf.d/instance.properties
%attr(0644,root,root) %{_sysconfdir}/aops/sync-conf.d/rdb/*
%attr(0755,root,root) %{_unitdir}/async-task.service
%{python3_sitelib}/async_task*.egg-info/*
%{python3_sitelib}/async_task/*
%attr(0755,root,root) %{_bindir}/async-task
%dir %attr(0644,root,root) /opt/aops/celery

%files -n zeus-distribute
%attr(0644,root,root) %{_sysconfdir}/aops/conf.d/zeus-distribute.yml
%attr(0755,root,root) %{_unitdir}/zeus-distribute.service
%{python3_sitelib}/zeus_distribute*.egg-info/*
%{python3_sitelib}/zeus/distribute_service/*

%changelog
* Fri Aug 23 2024 wenxin<wenxin32@foxmail.com> - v2.0.0-2
- updated download host template api to support english

* Fri Aug 16 2024 wenxin<wenxin32@foxmail.com> - v2.0.0-1
- Split the existing service module into distinct modules:
  - Task Module: Handles task management and execution.
  - Host Module: Manages host configurations and interactions.
  - User Module: Manages user accounts and permissions.
  - Interface Dispatch Module: Manages the dispatching of requests to appropriate interfaces.
- Added a new command line tool for enhanced functionality and management.
- Introduced support for clustering, allowing for more robust and scalable deployments.
- Reorganized the service architecture to improve modularity and maintainability.
- Fixed various issues to enhance stability and performance.


* Mon Jul 01 2024 smjiao<smjiao@isoftstone.com> - v1.4.0-9
- file trace interface

* Mon Jul 01 2024 smjiao<smjiao@isoftstone.com> - v1.4.0-8
- conf trace sync interface optimization

* Thu Mar 07 2024 wenxin<wenxin32@foxmail.com> - v1.4.0-7
- fix command injection vulnerabilities

* Wed Dec 27 2023 wenxin<wenxin32@foxmail.com> - v1.4.0-6
- update the exception catching type of the function

* Sun Dec 24 2023 luxuexian<luxuexian@huawei.com> - v1.4.0-5
- fix error log when query host status

* Thu Dec 21 2023 wenxin<wenxin32@foxmail.com> - v1.4.0-4
- update verification method for host ip field
- check host status when query host detail

* Tue Dec 19 2023 wenxin<wenxin32@foxmail.com> - v1.4.0-3
- update verification method for adding host or updating host info
- fix apollo TimedCorrectTask

* Mon Dec 18 2023 wenxin<wenxin32@foxmail.com> - v1.4.0-2
- Add interface for detecting host status.
- Update query host list api, add a new query method based on host name for it.
- Add rollback task execution method.
- Fix cve scan callback error.

* Tue Dec 12 2023 wenxin<wenxin32@foxmail.com> - v1.4.0-1
- Change CVE rollback task to hotpatch remove

* Mon Nov 06 2023 wenxin<wenxin32@foxmail.com> - v1.3.1-5
- Bugfix: email config does not take effect
- Add an interface for querying file list

* Fri Oct 27 2023 liulei<450962@qq.com> - v1.3.1-4
- Bugfix: update host api request error when changing username

* Thu Oct 26 2023 wenxin<wenxin32@foxmail.com> - v1.3.1-3
- update the template file contents for adding hosts

* Wed Oct 18 2023 wenxin<wenxin32@foxmail.com> - v1.3.1-2
- fix bug: metric proxy init failed
- add a way about key authentication for add host api
- remove python3-prometheus-api-client

* Thu Sep 21 2023 wenxin<shusheng.wen@outlook.com> - v1.3.1-1
- update spec requires
- update version info in setup.py file

* Tue Sep 19 2023 wenxin<shusheng.wen@outlook.com> - v1.3.0-4
- bugfix: cve fix result parsing error

* Tue Sep 19 2023 wenxin<shusheng.wen@outlook.com> - v1.3.0-3
- update callback request headers
- fix bash file sync error

* Wed Sep 13 2023 wenxin<shusheng.wen@outlook.com> - v1.3.0-2
- add file sync func

* Tue Sep 05 2023 wenxin<shusheng.wen@outlook.com> - v1.3.0-1
- optimize the method of executing apollo tasks

* Fri Jun 02 2023 wenxin<shusheng.wen@outlook.com> - v1.2.1-2
- update cve fix

* Tue May 23 2023 wenxin<shusheng.wen@outlook.com> - v1.2.1-1
- add cve rollback api;update cve scan callback func

* Tue May 09 2023 wenxin<shusheng.wen@outlook.com> - v1.2.0-4
- update validate rules of username field

* Mon May 08 2023 wenixn<shusheng.wen@outlook.com> - v1.2.0-3
- add gevent config item for uwsgi

* Thu Apr 27 2023 wenixn<shusheng.wen@outlook.com> - v1.2.0-2
- Fix token is not invalidated after the token was refreshed
- update args validation rules for add account and for add host
- replace thread scheduling with gevent scheduling when add host by batch

* Mon Apr 17 2023 wenixn<shusheng.wen@outlook.com> - v1.2.0-1
- update the call method of ceres; add function how to add host from web
- add api: update host info

* Tue Dec 27 2022 wenxin<shusheng.wen@outlook.com> - v1.1.1-4
- Modify uwsgi configuration file fields

* Wed Dec 21 2022 gongzhengtang<gong_zhengtang@163.com> - v1.1.1-3
- disabled mysql installed checked

* Tue Dec 06 2022 wenxin<shusheng.wen@outlook.com> - v1.1.1-2
- update delete host, remove the judgment about the workflow

* Fri Dec 02 2022 wenxin<shusheng.wen@outlook.com> - v1.1.1-1
- set timeout for cve scan,cve fix ,repo set

* Fri Nov 25 2022 wenxin<shusheng.wen@outlook.com> - v1.1.0-1
- remove test cases that use the responses module
- remove check_es_installed
- add cve cve fix, add cve cve scan

* Tue Nov 22 2022 zhuyuncheng<zhuyuncheng@huawei.com> - v1.0.0-1
- Package init

